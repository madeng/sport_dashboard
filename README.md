# Football Dashboard

**Football Dashboard** is intended to replace the big old score boards used in football matches. It keeps track of both the goals and the players who scored for each teams.

It can be used, not only for Football, but also for any sports opposing 2 teams : Hockey, Baseball, Softball, Handball, etc. The current implementation could easily be extended to support sports that involve more than two teams.

---
## Requirements

- [Java Run Time (JRE) version 8](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
- [Gradle 3.1](https://gradle.org/gradle-download/)
- [Git bash](https://git-for-windows.github.io/)
- A little bit of knowledge of command line

## How to run
1. Clone the repository and move at the root of the project
```sh
$ git clone 
```

2. Compile the code from the root of the project with Gradle 3.x
```sh
$ gradle build
```

3. Run the jar with : java -jar sport_dashboard-0.1.0.jar
```sh
$ java -jar build/libs/sport_dashboard-0.1.0.jar
```

## Commands

The commands that can be executed are :

1. Start a game between two teams
```sh
start: Team1 vs Team2
```

2. Count a goal done by the player <player> for the team <team> at the minute <min> of the match
```sh
<min> <team> <player>
```

3. Shows the current score of the match
```sh
print
```

4. End the current game
```sh
end
```

5. Shows the help
```sh
help
```

---
## The internals
This part explains the way the code was organized. 

### Packages 
The code has been organized in three main packages :
1. A package containing the starting point of the application (main)
2. A package holding the entities related to a game
3. A package keeping the commands that can be executed on a game

There is also a fourth package to keep the exceptions that can be raised in the application.

### Classes
![Class diagram](https://gitlab.com/madeng/sport_dashboard/raw/20905ff91e3d137c0710873dd5e51c5582b85065/dashboardClassDiagram.png)

There is two design patterns that stand out the most in this application (...beside the singleton)

State pattern
: The state pattern keeps track of the status of a game. In each state, a list of commands is available to the user, so it is only possible for a user to perform commands that are available into a particular state of the game.

Command pattern
: All the commands are implementation of the same interface. This way, it is easy to implement new commands. Once implemented, a command only needs to be assigned to the state(s) of a game for which it makes sense.

There is also a facade pattern that was used to make the testing easier in the GameCommandProcessor. This simple facade receives the user input from a String containing the command to execute. It processes one command at a time, so the output for each command can be scrutinized during the testing to perform the assertions.

Beside the design patterns, there are also interesting bit of data processing and Java features used in this application. Indeed, GameCommandProcessor makes use of a custom StringTokenizer to separate each commands into small portion that can be later analyzed by each Commands to determine if the command matches their function or not. It makes it a lot easier to understand the parser. It even supports double and single quoted strings.

Another mildly interesting piece of code was in the use of varargs in the GameStatus enum. To make it possible to specify as many commands for a game status as you wish, the varargs was a perfect choice. Also, since varargs is an optional argument, in order to enforce that each game status has a least one command available, the constructor was made with two arguments : the first is a command which is not optional and the second is the varargs if more commands need to be available in that particular game status.

Other facts behind the implementation :

- In GameCommandProcessing
	- The attribute Game is package-private to make it possible to scrutinize its content from the test packages
	- Since there is no game at the start and that the Game constructor requires the two teams to be provided, it was necessary to create a default state in GameStatus that is possible to retrieve statically. This default GameStatus is used to know which commands can be executed when there is no ongoing game . It renders the state pattern pretty useless in its current state, but in a future implementation, the application could be made to support a list of games in which each games would maintain their own state and even to have game schedule in at a later time. In this future insight, it would be useful to keep the state pattern.

- In Game
	- the list of goals are stored by team to be able to retrieved the information for each team separately
	- it was necessary to use a LinkedHashMap to be able to keep the order of insertion of the elements in the list to remember which team is the first one (home team) and which one is the second one (away team)

- Javadoc
    - The javadoc is being validated when the build is executed in gradle

---
## Known limitations
- Only one game can be active at any given time
- There is no history of the games that occurred in the past
- Works only for sports opposing 2 teams
- Can only counts goals by increment of 1 point. Basketball is a good example that would not work with this

## Tools
- Eclipse Neon
- Eclipse plugin EclEmma for code coverage
- Gradle 3.1
- JAVA 8 (JDK 1.8.0_101)
- [ObjectAid](http://www.objectaid.com/) for class diagram
- TestNG for unit testing
- GitLab (https://gitlab.com/madeng/sport_dashboard/)