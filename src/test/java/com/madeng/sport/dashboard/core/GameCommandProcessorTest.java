package com.madeng.sport.dashboard.core;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.testng.annotations.Test;

@Test
public class GameCommandProcessorTest {

	private static final String teamName1 = "Black Hawks";
	private static final String teamName2 = "Red Socks";

	private static final int time1 = 5;
	private static final int time2 = 15;
	private static final int time3 = 30;

	private static final String playerName1 = "Gonzalez";
	private static final String playerName2 = "Rodriguez";
	private static final String playerName3 = "Vazquez";

	private static final String COMMAND_PRINT_GAME_STATUS = "print";

	private static final String COMMAND_START_GAME = "Start: '" + teamName1 + "' vs. '" + teamName2 + "' ";

	private static final String COMMAND_STOP_GAME = "End";

	private static final String COMMAND_ADD_GOAL_1 = time1 + " '" + teamName1 + "' " + playerName1;
	private static final String COMMAND_ADD_GOAL_2 = time2 + " '" + teamName1 + "' " + playerName2;
	private static final String COMMAND_ADD_GOAL_3 = time3 + " '" + teamName2 + "' " + playerName3;
	private static final String COMMAND_ADD_GOAL_WITH_TIME_NOT_A_NUMBER = "notANumber '" + teamName2 + "' "
			+ playerName3;

	@Test(description = "Test normal flow of starting a game, making a couple of goals and ending a game")
	public void processCommand_withNormalFlow_expectSuccess() {
		GameCommandProcessor gameCommandProcessorUUT = new GameCommandProcessor();

		// Start Game
		gameCommandProcessorUUT.processCommand(COMMAND_START_GAME);
		String output = gameCommandProcessorUUT.processCommand(COMMAND_PRINT_GAME_STATUS);

		Assert.assertEquals(GameStatus.ONGOING, gameCommandProcessorUUT.game.getStatus());
		Assert.assertEquals(teamName1 + " 0 vs. " + teamName2 + " 0", output);

		// 1st goal for team 1
		gameCommandProcessorUUT.processCommand(COMMAND_ADD_GOAL_1);
		output = gameCommandProcessorUUT.processCommand(COMMAND_PRINT_GAME_STATUS);

		Assert.assertEquals(teamName1 + " 1 (" + playerName1 + " " + time1 + "') vs. " + teamName2 + " 0", output);

		// 2nd goal for team 1
		gameCommandProcessorUUT.processCommand(COMMAND_ADD_GOAL_2);
		output = gameCommandProcessorUUT.processCommand(COMMAND_PRINT_GAME_STATUS);

		Assert.assertEquals(teamName1 + " 2 (" + playerName1 + " " + time1 + "' " + playerName2 + " " + time2
				+ "') vs. " + teamName2 + " 0", output);

		// 1st goal for team 2
		gameCommandProcessorUUT.processCommand(COMMAND_ADD_GOAL_3);
		output = gameCommandProcessorUUT.processCommand(COMMAND_PRINT_GAME_STATUS);

		Assert.assertEquals(teamName1 + " 2 (" + playerName1 + " " + time1 + "' " + playerName2 + " " + time2
				+ "') vs. " + teamName2 + " 1 (" + playerName3 + " " + time3 + "')", output);

		output = gameCommandProcessorUUT.processCommand(COMMAND_STOP_GAME);

		Assert.assertEquals("The game has reached the end.\n" + teamName1 + " 2 (" + playerName1 + " " + time1 + "' "
				+ playerName2 + " " + time2
				+ "') vs. " + teamName2 + " 1 (" + playerName3 + " " + time3 + "')", output);
	}

	@Test(description = "Test trying to make a goal with a time that is not a number")
	public void processCommand_goalWithTimeAsNotANumber_expectNotRecognizedCommand() {
		GameCommandProcessor gameCommandProcessorUUT = new GameCommandProcessor();

		// Start Game
		gameCommandProcessorUUT.processCommand(COMMAND_START_GAME);
		gameCommandProcessorUUT.processCommand(COMMAND_PRINT_GAME_STATUS);

		String output = gameCommandProcessorUUT.processCommand(COMMAND_ADD_GOAL_WITH_TIME_NOT_A_NUMBER);

		// Validation
		String expectedOutput = "input error - please type 'print' for game details";
		assertEquals(expectedOutput, output);
	}

	@Test(description = "Test trying to make a goal when the game is not started")
	public void processCommand_goalWithoutAGame_expectErrorMessage() {
		GameCommandProcessor gameCommandProcessorUUT = new GameCommandProcessor();

		String output = gameCommandProcessorUUT.processCommand(COMMAND_ADD_GOAL_1);

		// Validation
		String expectedOutput = "No game currently in progress";
		assertEquals(expectedOutput, output);
	}

	@Test(description = "Test with mistaken command when a game is not started")
	public void processCommand_wrongCommandWithoutAGame_expectErrorMessage() {
		GameCommandProcessor gameCommandProcessorUUT = new GameCommandProcessor();

		String output = gameCommandProcessorUUT.processCommand("WrongCommand");

		// Validation
		String expectedOutput = "input error - please start a game through typing 'Start: '<Name of Home Team>' vs. '<Name of Away Team>'.";
		assertEquals(expectedOutput, output);
	}
}
