package com.madeng.sport.dashboard.core;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.madeng.sport.dashboard.core.exception.InvalidArgumentException;

/**
 * Test class {@link Game}
 *
 * @author math
 */
@Test
public class GameTest {

	private static final String TEAM_NAME_1 = "Tampa Bay";
	private static final String TEAM_NAME_2 = "Canadian";
	private static final String WRONG_TEAM_NAME_3 = "Blue Jays";

	private static final String PLAYER_NAME_1 = "Martin Brodeur";
	private static final String PLAYER_NAME_2 = "Vincent Damphousse";

	private static final int TIME_OF_GOAL_1 = 15;
	private static final int TIME_OF_GOAL_2 = 30;
	private static final int WRONG_TIME_OF_GOAL_2 = -1;

	@Test(description = "Test getting goals right after initialization")
	public void getGoals_rightAfterInitialization_expectNoGoals() throws InvalidArgumentException {
		Game game = new Game(TEAM_NAME_1, TEAM_NAME_2);
		Assert.assertTrue(game.getGoals(TEAM_NAME_1).isEmpty(), "The list of goals should be empty");
		Assert.assertTrue(game.getGoals(TEAM_NAME_2).isEmpty(), "The list of goals should be empty");
	}

	@Test(description = "Test adding goals with normal conditions satisfied")
	public void addGoal_toBothTeams_expectGoalsToBeCumulated() throws InvalidArgumentException {
		Game game = new Game(TEAM_NAME_1, TEAM_NAME_2);

		game.addGoal(TEAM_NAME_1, PLAYER_NAME_1, TIME_OF_GOAL_1);
		game.addGoal(TEAM_NAME_2, PLAYER_NAME_2, TIME_OF_GOAL_2);
		game.addGoal(TEAM_NAME_2, PLAYER_NAME_1, TIME_OF_GOAL_2);

		// Verification
		List<Goal> goalsTeam1 = game.getGoals(TEAM_NAME_1);
		List<Goal> goalsTeam2 = game.getGoals(TEAM_NAME_2);
		Assert.assertEquals(goalsTeam1.size(), 1, "Expect to have one goal for team 1");
		Assert.assertEquals(goalsTeam2.size(), 2, "Expect to have two goals for team 2");
	}

	@Test(description = "Test adding a goal for a team that does not exist", expectedExceptions = InvalidArgumentException.class)
	public void addGoal_toATeamThatDoesNotExist_expectedFailure() throws InvalidArgumentException {
		Game game = new Game(TEAM_NAME_1, TEAM_NAME_2);

		game.addGoal(WRONG_TEAM_NAME_3, PLAYER_NAME_1, TIME_OF_GOAL_1);
	}

	@Test(description = "Test adding a goal with negative time", expectedExceptions = InvalidArgumentException.class)
	public void addGoal_withNegativeTime_expectedFailure() throws InvalidArgumentException {
		Game game = new Game(TEAM_NAME_1, TEAM_NAME_2);

		game.addGoal(TEAM_NAME_2, PLAYER_NAME_1, WRONG_TIME_OF_GOAL_2);
	}
}
