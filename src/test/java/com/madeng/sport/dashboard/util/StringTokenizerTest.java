package com.madeng.sport.dashboard.util;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Test for class {@link StringTokenizer}
 *
 * @author math
 */
@Test
public class StringTokenizerTest {

	@DataProvider(name = "tokenizer")
	public static Object[][] tokenizerDataProvider() {
		return new Object[][] {
				new Object[] { "Hello World!",
						Arrays.asList("Hello", "World!") },
				new Object[] { "String with \"double-quoted String\" and 'single-quoted String'",
						Arrays.asList("String", "with", "double-quoted String", "and", "single-quoted String") },
				new Object[] { "A \"Double-quoted String with 'single quote' string inside\"",
						Arrays.asList("A", "Double-quoted String with 'single quote' string inside") },
				new Object[] { "Some 'single-quoted String with \"double quote\" string inside'",
						Arrays.asList("Some", "single-quoted String with \"double quote\" string inside") },
				new Object[] { "String with colon: 'single quote' and dot.",
						Arrays.asList("String", "with", "colon:", "single quote", "and", "dot.") }
		};
	}

	@Test(dataProvider = "tokenizer")
	public void tokenize(String input, List<String> expectedTokens) {
		List<String> tokens = StringTokenizer.tokenize(input);
		Assert.assertThat(tokens, IsIterableContainingInAnyOrder
				.containsInAnyOrder(expectedTokens.toArray(new String[expectedTokens.size()])));
	}
}
