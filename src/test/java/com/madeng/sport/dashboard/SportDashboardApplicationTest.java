package com.madeng.sport.dashboard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.junit.Assert;
import org.testng.annotations.Test;

@Test
public class SportDashboardApplicationTest {

	private static final String testResourcesFolder = "src/test/resources/";

	// Unit under test
	private SportDashboardApplication sportDashboardAppUUT = new SportDashboardApplication();

	@Test(description = "Test that commands can be read correctly from the main application and that it can be stopped")
	public void startAndStopGame_withCorrectInput_expectSuccess() throws IOException {
		FileReader fileReader;
		try {
			fileReader = new FileReader(new File(testResourcesFolder + "/startStopGame.txt"));
		} catch (FileNotFoundException e) {
			Assert.fail("Test file was not found: " + e.getMessage());
			return;
		}
		Writer writer = new StringWriter();
		sportDashboardAppUUT.readInput(fileReader, writer);

		// Validate output
		String expectedOutput = "Welcome to the Scoring Dashboard!\n"
				+ "At any time, type 'exit()' to exit the application\n"
				+ "\n"
				+ "Type a command: \n"
				+ "A game has been started!\n"
				+ "\n"
				+ "Type a command: \n"
				+ "The game has reached the end.\n"
				+ "England 0 vs. West Germany 0\n"
				+ "\n"
				+ "Type a command: \n"
				+ "Exiting the application...\n"
				+ "Done\n";
		Assert.assertEquals(expectedOutput, writer.toString());
	}
}
