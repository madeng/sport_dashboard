package com.madeng.sport.dashboard.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Custom made String tokenizer to deal with quoted tokens
 *
 * @author math
 */
public final class StringTokenizer {

	private StringTokenizer() {
		// nothing to do
	}

	/**
	 * Tokenize a String into words or groups of words.
	 *
	 * Support single or double quoted strings. The leading and trailing quotes are removed if the token is a group of words
	 *
	 * @param input
	 *            an input String
	 * @return the list of tokens extracted from the input
	 */
	public static List<String> tokenize(final String input) {
		List<String> tokens = new ArrayList<>();
		// Inspired from : http://stackoverflow.com/a/7804472
		Matcher m = Pattern.compile("([^\"']\\S*|'.+?'|\".+?\")\\s*").matcher(input);
		while (m.find()) {
			tokens.add(m.group(1).replaceAll("^(\"(.+)\"|'(.+)')$", "$2$3"));
		}
		return tokens;
	}
}
