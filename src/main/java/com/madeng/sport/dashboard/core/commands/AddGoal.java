package com.madeng.sport.dashboard.core.commands;

import java.util.List;

import com.madeng.sport.dashboard.core.Game;
import com.madeng.sport.dashboard.core.exception.BadStateException;
import com.madeng.sport.dashboard.core.exception.InvalidArgumentException;
import com.madeng.sport.dashboard.core.exception.UnexpectedRunTimeException;
import com.madeng.sport.dashboard.core.exception.UnrecognizedCommandException;

/**
 * Command to add a goal to the game
 *
 * @author math
 */
public final class AddGoal implements Command {

	private static AddGoal instance = null;

	public static AddGoal getInstance() {
		if (instance == null) {
			instance = new AddGoal();
		}
		return instance;
	}

	@Override
	public boolean isPatternMatching(List<String> tokens) {
		if (tokens.size() != 3) {
			return false;
		}
		try {
			int time = Integer.parseInt(tokens.get(0));
			if (time < 0) {
				// Time cannot be negative
				return false;
			}
		} catch (NumberFormatException e) {
			// The first token is not a number
			return false;
		}
		return true;
	}

	@Override
	public CommandOutput execute(Game game, List<String> tokens) {
		if (!isPatternMatching(tokens)) {
			throw new UnrecognizedCommandException("Goal could not be counted as the command was not recognized");
		}
		if (game == null) {
			throw new BadStateException("Goal can only be added to a game that is ongoing!");
		}
		int minuteOfMatch = Integer.parseInt(tokens.get(0));
		String scoringTeamName = tokens.get(1);
		String playerName = tokens.get(2);
		try {
			game.addGoal(scoringTeamName, playerName, minuteOfMatch);
		} catch (InvalidArgumentException e) {
			throw new UnexpectedRunTimeException("The game does not have a team with name :" + scoringTeamName, e);
		}
		String message = ShowGameStatus.getInstance().execute(game, tokens).getMessage();
		return new CommandOutput(game, message);
	}

	@Override
	public String getInfo() {
		return "To mark a goal, type: \"<minute> '<Team>' <name of scorer>\"";
	}

}
