package com.madeng.sport.dashboard.core.commands;

import java.util.Arrays;
import java.util.List;

import com.madeng.sport.dashboard.core.Game;

/**
 * A command that is applicable on some input
 *
 * @author math
 */
public interface Command {

	/**
	 * Check if the input is matching the pattern of this command
	 *
	 * @param tokens
	 *            The tokens that are given information about the command
	 * @return true if the tokens correspond to this command pattern
	 */
	boolean isPatternMatching(List<String> tokens);

	/**
	 * Execute this command with a tokenized input.
	 *
	 * @param game
	 *            A sport game
	 * @param token
	 *            the list of tokens describing the command to execute
	 * @return a {@link CommandOutput} object
	 */
	CommandOutput execute(Game game, List<String> token);

	/**
	 * Get the information about this command
	 *
	 * @return a command description
	 */
	String getInfo();

	/**
	 * Get all the existing commands
	 *
	 * @return all the existing commands
	 */
	static List<Command> getAllCommands() {
		return Arrays.asList(StartGame.getInstance(), AddGoal.getInstance(), EndGame.getInstance(),
				ShowGameStatus.getInstance());
	}
}
