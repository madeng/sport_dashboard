package com.madeng.sport.dashboard.core.commands;

import com.madeng.sport.dashboard.core.Game;

/**
 * Contains the output of a command
 *
 * @author math
 */
public class CommandOutput {

	protected final Game game;

	protected final String message;

	protected final boolean hasError;

	public CommandOutput(Game game, String message) {
		this(game, message, false);
	}

	public CommandOutput(Game game, String message, boolean hasError) {
		this.game = game;
		this.message = message;
		this.hasError = hasError;
	}

	public Game getGame() {
		return game;
	}

	public String getMessage() {
		return message;
	}

	public boolean isHasError() {
		return hasError;
	}
}
