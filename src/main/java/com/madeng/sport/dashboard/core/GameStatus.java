package com.madeng.sport.dashboard.core;

import java.util.ArrayList;
import java.util.List;

import com.madeng.sport.dashboard.core.commands.AddGoal;
import com.madeng.sport.dashboard.core.commands.Command;
import com.madeng.sport.dashboard.core.commands.EndGame;
import com.madeng.sport.dashboard.core.commands.ShowGameStatus;
import com.madeng.sport.dashboard.core.commands.ShowHelp;
import com.madeng.sport.dashboard.core.commands.StartGame;

/**
 * Possible game statuses. It also keeps the commands that are available in each statuses
 *
 * @author math
 */
public enum GameStatus {

	AWAITING(StartGame.getInstance()), ONGOING(AddGoal.getInstance(), EndGame.getInstance(), ShowGameStatus
			.getInstance());

	private final List<Command> availableCommands = new ArrayList<>();

	public static GameStatus getDefaultStatus() {
		return GameStatus.AWAITING;
	}

	/**
	 * Constructor with at least one command that can be executed in this state
	 *
	 * @param command
	 *            A command that can be performed in this state
	 * @param commands
	 *            Optionally more commands that can be performed in this state
	 */
	GameStatus(final Command command, final Command... commands) {
		availableCommands.add(command);
		availableCommands.add(ShowHelp.getInstance());
		for (Command commandTmp : commands) {
			availableCommands.add(commandTmp);
		}
	}

	public List<Command> getAvailableCommands() {
		return availableCommands;
	}
}
