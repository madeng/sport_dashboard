package com.madeng.sport.dashboard.core;

import java.util.List;

import com.madeng.sport.dashboard.core.commands.Command;
import com.madeng.sport.dashboard.core.commands.CommandOutput;
import com.madeng.sport.dashboard.util.StringTokenizer;

/**
 * Handle the commands that are affecting the Game status and dashboard
 *
 * @author math
 */
public class GameCommandProcessor {

	/* package-private */ Game game;

	/**
	 * Process a command that affects a Game
	 *
	 * @param inputCmd
	 *            the input command to execute on a game
	 * @return a String containing the information the command output
	 */
	public String processCommand(String inputCmd) {
		// Clean up the input command of extra spaces
		inputCmd = inputCmd.trim().replaceAll(" +", " ");
		GameStatus gameStatus;
		if (game != null) {
			gameStatus = game.getStatus();
		} else {
			gameStatus = GameStatus.getDefaultStatus();
		}
		List<String> tokens = StringTokenizer.tokenize(inputCmd);
		CommandOutput commandOutput = null;
		// Search for a command that matches the input
		for (Command cmd : gameStatus.getAvailableCommands()) {
			if (cmd.isPatternMatching(tokens)) {
				commandOutput = cmd.execute(game, tokens);
				break;
			}
		}
		String outputMessage;
		if (commandOutput != null) {
			// The command WAS recognized
			game = commandOutput.getGame();
			outputMessage = commandOutput.getMessage();
		} else {
			if (gameStatus == GameStatus.AWAITING) {
				// If there is no current game, check if the command exists or not
				// and show a specific message for both case.
				boolean isValidCommand = Command.getAllCommands().stream()
						.anyMatch(cmd -> cmd.isPatternMatching(tokens));
				if (isValidCommand) {
					outputMessage = "No game currently in progress";
				} else {
					outputMessage = "input error - please start a game through typing 'Start: '<Name of Home Team>' vs. '<Name of Away Team>'.";
				}
			} else {
				// Show a message if the command was not matching any known commands
				outputMessage = "input error - please type 'print' for game details";
			}
		}
		return outputMessage;
	}
}
