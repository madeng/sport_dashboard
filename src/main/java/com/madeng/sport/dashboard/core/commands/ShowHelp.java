package com.madeng.sport.dashboard.core.commands;

import java.util.List;

import com.madeng.sport.dashboard.core.Game;
import com.madeng.sport.dashboard.core.GameStatus;
import com.madeng.sport.dashboard.core.exception.BadStateException;

public final class ShowHelp implements Command {

	private static ShowHelp instance = null;

	public static ShowHelp getInstance() {
		if (instance == null) {
			instance = new ShowHelp();
		}
		return instance;
	}

	@Override
	public boolean isPatternMatching(List<String> tokens) {
		if (tokens.size() != 1) {
			return false;
		}
		if (tokens.get(0).equalsIgnoreCase("help")) {
			return true;
		}
		return false;
	}

	@Override
	public CommandOutput execute(Game game, List<String> token) {
		if (!isPatternMatching(token)) {
			throw new BadStateException("Wrong state");
		}
		StringBuffer helpOutput = new StringBuffer(
				"Here are the commands that can be performed :\n");
		GameStatus status;
		if (game != null) {
			status = game.getStatus();
		} else {
			status = GameStatus.getDefaultStatus();
		}
		for (Command command : status.getAvailableCommands()) {
			helpOutput.append("\t");
			helpOutput.append(command.getInfo());
			helpOutput.append("\n");
		}
		return new CommandOutput(game, helpOutput.toString());
	}

	@Override
	public String getInfo() {
		return "Show help by typing 'help'";
	}

}
