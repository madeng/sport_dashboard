package com.madeng.sport.dashboard.core.exception;

public class UnexpectedRunTimeException extends RuntimeException {

	private static final long serialVersionUID = 1661473386005310305L;

	public UnexpectedRunTimeException(String msg) {
		super(msg);
	}

	public UnexpectedRunTimeException(String msg, Throwable e) {
		super(msg, e);
	}

	public UnexpectedRunTimeException(Throwable e) {
		super(e);
	}
}
