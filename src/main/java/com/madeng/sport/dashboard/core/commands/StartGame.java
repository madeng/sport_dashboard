package com.madeng.sport.dashboard.core.commands;

import java.util.List;

import com.madeng.sport.dashboard.core.Game;
import com.madeng.sport.dashboard.core.GameStatus;
import com.madeng.sport.dashboard.core.exception.UnrecognizedCommandException;

/**
 * Command to start a match
 */
public class StartGame implements Command {

	private static StartGame instance = null;

	public static StartGame getInstance() {
		if (instance == null) {
			instance = new StartGame();
		}
		return instance;
	}

	private StartGame() {
		// Nothing to do
	}

	@Override
	public boolean isPatternMatching(List<String> tokens) {
		if (tokens.size() != 4) {
			return false;
		}
		return tokens.get(0).equalsIgnoreCase("start:");
	}

	@Override
	public CommandOutput execute(Game game, List<String> tokens) {
		if (!isPatternMatching(tokens)) {
			throw new UnrecognizedCommandException(
					"Starting the match was not possible as the command was not recognized");
		}
		String homeTeamName = tokens.get(1);
		String awayTeamName = tokens.get(3);
		Game newGame = new Game(homeTeamName, awayTeamName);
		newGame.setStatus(GameStatus.ONGOING);
		return new CommandOutput(newGame, "A game has been started!");
	}

	@Override
	public String getInfo() {
		return "Start a game by typing \"Start: 'Name of Home Team' vs 'Name of Away Team'\"";
	}
}
