package com.madeng.sport.dashboard.core.exception;

public class InvalidArgumentException extends Exception {

	private static final long serialVersionUID = 684782524256316777L;

	public InvalidArgumentException(String msg) {
		super(msg);
	}

	public InvalidArgumentException(String msg, Throwable e) {
		super(msg, e);
	}

	public InvalidArgumentException(Throwable e) {
		super(e);
	}
}
