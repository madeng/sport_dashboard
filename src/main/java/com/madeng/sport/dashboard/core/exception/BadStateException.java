package com.madeng.sport.dashboard.core.exception;

public class BadStateException extends RuntimeException {
	private static final long serialVersionUID = -5062513630859053726L;

	public BadStateException(String msg) {
		super(msg);
	}

	public BadStateException(String msg, Throwable e) {
		super(msg, e);
	}

	public BadStateException(Throwable e) {
		super(e);
	}
}
