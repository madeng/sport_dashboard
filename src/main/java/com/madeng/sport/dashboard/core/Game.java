package com.madeng.sport.dashboard.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.madeng.sport.dashboard.core.exception.InvalidArgumentException;

public class Game {

	/**
	 * Contains the list of goals for each team. Index is the team name
	 */
	protected final Map<String, List<Goal>> goalsByTeam = new LinkedHashMap<>();

	protected GameStatus status;

	public Game(final String teamName1, final String teamName2) {
		goalsByTeam.put(teamName1, new ArrayList<>(0));
		goalsByTeam.put(teamName2, new ArrayList<>(0));
		status = GameStatus.AWAITING;
	}

	public Collection<String> getTeams() {
		return goalsByTeam.keySet();
	}

	public GameStatus getStatus() {
		return status;
	}

	public void setStatus(GameStatus status) {
		this.status = status;
	}

	/**
	 * Add a new goal to the team who did the point
	 *
	 * @param teamName
	 *            The name of the team that made a goal
	 * @param playerName
	 *            The name of the player who performed the goal
	 * @param timeOfGoal
	 *            time of the goal in minute since the beginning of the game
	 * @throws InvalidArgumentException
	 *             If the name of the team does not correspond to a team that is playing in this game or if the time of the goal is negative
	 */
	public void addGoal(String teamName, String playerName, int timeOfGoal) throws InvalidArgumentException {
		if (timeOfGoal < 0) {
			throw new InvalidArgumentException("Time of goal should be above 0, but received : " + timeOfGoal);
		}
		if (!goalsByTeam.containsKey(teamName)) {
			throw new InvalidArgumentException("Team " + teamName + " is not playing right now");
		}
		goalsByTeam.get(teamName).add(new Goal(playerName, timeOfGoal));
	}

	/**
	 * Get the list of goals for one of the team
	 *
	 * @param teamName
	 *            the name of the team for which to get the list of goals
	 * @return the list of goals for the requested team
	 *
	 * @throws InvalidArgumentException
	 *             If the name of the team does not correspond to a team that is playing in this game
	 */
	public List<Goal> getGoals(String teamName) throws InvalidArgumentException {
		if (!goalsByTeam.containsKey(teamName)) {
			throw new InvalidArgumentException("Team " + teamName + " is not playing right now");
		}
		return goalsByTeam.get(teamName);
	}
}
