package com.madeng.sport.dashboard.core.commands;

import java.util.List;

import com.madeng.sport.dashboard.core.Game;

/**
 * Command to stop a match
 *
 * @author math
 *
 */
public final class EndGame implements Command {

	private static EndGame instance = null;

	public static EndGame getInstance() {
		if (instance == null) {
			instance = new EndGame();
		}
		return instance;
	}

	private EndGame() {
		// Nothing to do
	}

	@Override
	public boolean isPatternMatching(List<String> tokens) {
		if (tokens.size() != 1) {
			return false;
		}
		return tokens.get(0).equalsIgnoreCase("end");
	}

	@Override
	public CommandOutput execute(Game game, List<String> inputCommand) {
		CommandOutput output = ShowGameStatus.getInstance().execute(game, null);
		String message = "The game has reached the end.\n" + output.getMessage();
		return new CommandOutput(null, message);
	}

	@Override
	public String getInfo() {
		return "End a game by typing : \"end\"";
	}

}
