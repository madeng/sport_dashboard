package com.madeng.sport.dashboard.core.exception;

public class UnrecognizedCommandException extends RuntimeException {

	private static final long serialVersionUID = -7380385850911009685L;

	public UnrecognizedCommandException(String msg) {
		super(msg);
	}

	public UnrecognizedCommandException(String msg, Throwable e) {
		super(msg, e);
	}

	public UnrecognizedCommandException(Throwable e) {
		super(e);
	}
}
