package com.madeng.sport.dashboard.core.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.madeng.sport.dashboard.core.Game;
import com.madeng.sport.dashboard.core.Goal;
import com.madeng.sport.dashboard.core.exception.InvalidArgumentException;
import com.madeng.sport.dashboard.core.exception.UnexpectedRunTimeException;

/**
 * Command to show the status of a game
 *
 * @author math
 */
public class ShowGameStatus implements Command {

	private static ShowGameStatus instance = null;

	public static ShowGameStatus getInstance() {
		if (instance == null) {
			instance = new ShowGameStatus();
		}
		return instance;
	}

	@Override
	public boolean isPatternMatching(List<String> tokens) {
		if (tokens.size() != 1) {
			return false;
		}
		return tokens.get(0).equalsIgnoreCase("print");
	}

	@Override
	public CommandOutput execute(Game game, List<String> inputCommand) {
		Collection<String> teamNames = game.getTeams();
		List<String> allTeamStats = new ArrayList<>();
		for (String teamName : teamNames) {
			List<Goal> goals;
			try {
				goals = game.getGoals(teamName);
			} catch (InvalidArgumentException e) {
				// Should not happen, since the teamName are retrieved above
				throw new UnexpectedRunTimeException("The game does not have a team with name :" + teamName, e);
			}
			StringBuffer allGoalsbyPlayer = new StringBuffer();
			if (!goals.isEmpty()) {
				List<String> playerGoals = new ArrayList<>();
				for (Goal goal : goals) {
					playerGoals.add(goal.getPlayerName() + " " + goal.getTimeOfGoal() + "'");
				}
				allGoalsbyPlayer.append(" (");
				allGoalsbyPlayer.append(String.join(" ", playerGoals));
				allGoalsbyPlayer.append(")");
			}
			StringBuffer teamStats = new StringBuffer();
			teamStats.append(teamName);
			teamStats.append(" ");
			teamStats.append(goals.size());
			teamStats.append(allGoalsbyPlayer);
			allTeamStats.add(teamStats.toString());
		}
		return new CommandOutput(game, String.join(" vs. ", allTeamStats));
	}

	@Override
	public String getInfo() {
		return "Type 'print' for game details";
	}

}
