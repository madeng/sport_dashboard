package com.madeng.sport.dashboard.core;

public final class Goal {
	/**
	 * The name of the player who scored
	 */
	private final String playerName;

	/**
	 * Keep the time of the goal since the beginning of the match (in minutes)
	 */
	private final int timeOfGoal;

	public Goal(String playerName, int timeOfGoal) {
		this.playerName = playerName;
		this.timeOfGoal = timeOfGoal;
	}

	public String getPlayerName() {
		return playerName;
	}

	public int getTimeOfGoal() {
		return timeOfGoal;
	}
}
