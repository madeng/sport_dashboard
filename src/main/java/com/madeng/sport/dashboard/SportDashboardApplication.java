package com.madeng.sport.dashboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import com.madeng.sport.dashboard.core.GameCommandProcessor;

public class SportDashboardApplication {

	private static final String EXIT_COMMAND = "exit()";

	public static void main(String[] args) throws IOException {
		// Read from input of the console
		try (Reader reader = new InputStreamReader(System.in); Writer writer = new OutputStreamWriter(System.out)) {
			new SportDashboardApplication().readInput(reader, writer);
		}
	}

	/**
	 * Main process that keeps listening for user input and write to the output
	 *
	 * @param reader
	 *            the interface for the user to give commands to the application
	 * @param output
	 *            the place where to write the output of the commands
	 * @throws IOException
	 *             if it is possible to write to the output
	 */
	public void readInput(Reader reader, Writer output) throws IOException {
		BufferedReader bufferReader = new BufferedReader(reader);
		GameCommandProcessor gameCommandProcessor = new GameCommandProcessor();
		output.write("Welcome to the Scoring Dashboard!\n");
		output.write("At any time, type '" + EXIT_COMMAND + "' to exit the application\n");
		boolean exitApplication = false;
		while (!exitApplication) {
			try {
				output.write("\nType a command: \n");
				output.flush();
				String inputCmd = bufferReader.readLine();
				if (inputCmd.trim().equalsIgnoreCase(EXIT_COMMAND)) {
					output.write("Exiting the application...\n");
					output.flush();
					exitApplication = true;
				} else if (!inputCmd.isEmpty()) {
					output.write(gameCommandProcessor.processCommand(inputCmd) + "\n");
					output.flush();
				}
			} catch (Exception e) {
				// Catch all exception to keep the application running
				output.write(e.getMessage() + "\n");
				output.flush();
			}
		}
		output.write("Done\n");
	}
}
